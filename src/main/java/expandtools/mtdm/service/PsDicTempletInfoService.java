package expandtools.mtdm.service;

import expandtools.mtdm.entity.PsDicField;
import expandtools.mtdm.entity.PsDicTable;
import expandtools.mtdm.entity.PsDicTemplet;
import expandtools.mtdm.entity.PsDicTempletInfo;
import expandtools.mtdm.mapper.PsDicFieldMapper;
import expandtools.mtdm.mapper.PsDicTableMapper;
import expandtools.mtdm.mapper.PsDicTempletInfoMapper;
import expandtools.mtdm.mapper.PsDicTempletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName PsDicTempletInfoService
 * @Description TODO 描写类的含义
 * @Author 消魂钉
 * @Date 6/6 0006 18:01
 */
@Service
@Transactional(readOnly = true)
public class PsDicTempletInfoService {
    @Resource
    private PsDicTempletInfoMapper psDicTempletInfoMapper;
    @Resource
    private PsDicTableMapper psDicTableMapper;
    @Resource
    private PsDicTempletMapper psDicTempletMapper;
    @Resource
    private PsDicFieldMapper psDicFieldMapper;

    public List<PsDicTempletInfo> select(PsDicTempletInfo psDicTempletInfo) {
//        psDicTempletInfo.setTempletid("de9d81c4f57a4eecbb22ab5686616875");
//        psDicTempletInfo.setTableid("0fed24dae3ac4f32bac080ceb645034f");

        //PsDicTemplet pdtInfo = psDicTempletMapper.selectByPrimaryKey(psDicTempletInfo.getTempletid());
        //psDicTempletInfo.setTableid(pdtInfo.getTableid());
        return psDicTempletInfoMapper.select(psDicTempletInfo);
    }

    public List<PsDicTable> getPsDicTableAll() {
        return psDicTableMapper.selectAll();
    }

    public List<PsDicTemplet> getPsDicTempletAll() {
        return psDicTempletMapper.selectAll();
    }

    public PsDicTable getPsPicTableId(String tableid) {
        return psDicTableMapper.selectByPrimaryKey(tableid);
    }

    @Transactional(readOnly = false)
    public void save(PsDicTempletInfo psDicTempletInfo) {
        psDicTempletInfoMapper.updateByPrimaryKeySelective(psDicTempletInfo);

    }

    public PsDicField selectPsDicField(String tableid, String fieldcode) {
        PsDicField psDicField = PsDicField.builder().tableid(tableid).code(fieldcode).build();
        PsDicField pdfield = psDicFieldMapper.selectByPrimaryKey(psDicField);
        return pdfield;
    }

    public PsDicTable selectTableName(String tableid) {
        return psDicTableMapper.selectByPrimaryKey(tableid);
    }

//    public void selectPsDicField(String tableid, String fieldcode) {
//        PsDicTable psDicTable = new PsDicTable();
//        psDicTable.setCode(fieldcode);
//        psDicTable.set
//
//        psDicFieldMapper.selectByPrimaryKey()
//    }
}
