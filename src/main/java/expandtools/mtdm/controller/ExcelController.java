package expandtools.mtdm.controller;

import cn.afterturn.easypoi.entity.vo.NormalExcelConstants;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import expandtools.mtdm.entity.PsDicTempletInfo;
import expandtools.mtdm.service.PsDicTempletInfoService;
import expandtools.mtdm.utils.ExcelExportMyStylerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @ClassName ExcelController
 * @Description TODO 描写类的含义
 * @Author 消魂钉
 * @Date 6/6 0006 19:48
 */
@Controller
@RequestMapping("/excel")
public class ExcelController {

    @Autowired
    private PsDicTempletInfoService psDicTempletInfoService;

    @PostMapping(value = "/template/excel")
    public String createMidTotalExcel(ModelMap map,PsDicTempletInfo PsDicTempletInfo) {
        try {
            List<PsDicTempletInfo> list= psDicTempletInfoService.select(PsDicTempletInfo);
            ExportParams params = new ExportParams("快速模板导入", "快速模板导入",
                    ExcelType.XSSF);
            map.put(NormalExcelConstants.DATA_LIST, list);
            /** 导出实体 */
            map.put(NormalExcelConstants.CLASS, PsDicTempletInfo.class);
            /** 参数 */
            map.put(NormalExcelConstants.PARAMS, params);
            /** 文件名称 */
            map.put(NormalExcelConstants.FILE_NAME, "修改模板下载");
        } catch (Exception e) {
            e.printStackTrace();
        }
        /** View名称 */
        return NormalExcelConstants.EASYPOI_EXCEL_VIEW;
    }

    @RequestMapping("/test")
    public String test(){
        return "index";
    }
}
