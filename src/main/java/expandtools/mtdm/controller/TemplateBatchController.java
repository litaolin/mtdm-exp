package expandtools.mtdm.controller;

import expandtools.mtdm.entity.*;
import expandtools.mtdm.service.PsDicTempletInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.Name;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName TemplateBatchController
 * @Description TODO 描写类的含义
 * @Author 消魂钉
 * @Date 6/6 0006 17:28
 */
@RestController
@RequestMapping("/ext/template")
public class TemplateBatchController {
    @Autowired
    private PsDicTempletInfoService psDicTempletInfoService;

    @RequestMapping("/show")
    public List<PsDicTempletInfo> test(PsDicTempletInfo PsDicTempletInfo){
        List<PsDicTempletInfo> list= psDicTempletInfoService.select(PsDicTempletInfo);
        if (!CollectionUtils.isEmpty(list)){
            list.forEach(c->{
                PsDicField psDicField = psDicTempletInfoService.selectPsDicField(c.getTableid(),c.getFieldcode());
                c.setFieldName(psDicField.getName());
                PsDicTable psDicTable = psDicTempletInfoService.selectTableName(c.getTableid());
                c.setTableName(psDicTable.getName());
            });
        }
        return list;
    }

    @RequestMapping("/dicTable")
    public List<Dic> dicTableList(PsDicTable psDicTable){
        List<PsDicTable> list = psDicTempletInfoService.getPsDicTableAll();
        List<Dic> dicList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)){
            list.forEach(c->{
                Dic dics = Dic.builder().label(c.getId() + ":" + c.getCode() + ":" + c.getName()).value(c.getId()).build();
                dicList.add(dics);
            });

        }
        return dicList;
    }
    @RequestMapping("/dicTempletId")
    public List<Dic> dicTempletList(PsDicTemplet psDicTemplet){
        List<PsDicTemplet> list = psDicTempletInfoService.getPsDicTempletAll();
        List<Dic> dicList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)){
            list.forEach(c->{
                PsDicTable psDicTable = psDicTempletInfoService.getPsPicTableId(c.getTableid());
                Dic dics = Dic.builder().label(c.getCode() + ":" + c.getName()+":("+psDicTable.getName()+":"+psDicTable.getCode()+")").value(c.getId()).build();
                dicList.add(dics);
            });

        }
        return dicList;
    }

    /**
     * saveTemplate
     */
    @RequestMapping("/saveTemplate")
    public String saveTemplate(@RequestBody PsDicTempletInfo psDicTempletInfo){

        if (StringUtils.isNotBlank(psDicTempletInfo.getTableid())
        &&StringUtils.isNotBlank(psDicTempletInfo.getTempletid())
                &&StringUtils.isNotBlank(psDicTempletInfo.getFieldcode())){
            psDicTempletInfoService.save(psDicTempletInfo);
        }
        return "成功";
    }

}
