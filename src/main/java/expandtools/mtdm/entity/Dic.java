package expandtools.mtdm.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName Dic
 * @Description TODO 描写类的含义
 * @Author 消魂钉
 * @Date 6/8 0008 7:44
 */
@Data
@NoArgsConstructor
public class Dic {

    private String label;

    private String value;
    @Builder
    public Dic(String label, String value) {
        this.label = label;
        this.value = value;
    }
}
