package expandtools.mtdm.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;

import javax.persistence.*;


@Table(name = "ps_dic_templet_info")
public class PsDicTempletInfo {
    @Excel(name = "模板编号(不可更改)")
    @Column(name = "TEMPLETID")
    @Id
    private String templetid;
    @Excel(name = "表编号(不可更改)")
    @Column(name = "TABLEID")
    @Id
    private String tableid;
    @Transient
    private String tableName;

    @Excel(name = "字段代码(不可更改)")
    @Column(name = "FIELDCODE")
    @Id
    private String fieldcode;
    @Transient
    private String fieldName;

    @Excel(name = "排序字段(不可更改)")
    @Column(name = "TABINDEX")
    @OrderBy(value="asc")
    private Double tabindex;
    @Excel(name = "名称(不可更改)")
    @Column(name = "NAME")
    private String name;
    @Excel(name = "长度")
    @Column(name = "LENGTH")
    private Double length;
    @Column(name = "PROPERTY")
    @Excel(name = "属性")
    private String property;

    @Column(name = "MIN_VALUE")
    private String minValue;

    @Column(name = "MAX_VALUE")
    private String maxValue;

    @Column(name = "FORMAT")
    private String format;

    @Column(name = "RESCODE")
    private String rescode;

    @Column(name = "SOURCE")
    private String source;

    @Column(name = "REALFIELD")
    private String realfield;

    @Column(name = "TAGTYPE")
    private String tagtype;

    @Column(name = "OPERATOR")
    private String operator;

    @Column(name = "ISEDIT")
    private String isedit;

    @Column(name = "ISREADONLY")
    private String isreadonly;

    @Column(name = "EDITEVENT")
    private String editevent;

    @Column(name = "ISLIST")
    private String islist;

    @Column(name = "ISDISPLAY")
    private String isdisplay;

    @Column(name = "ISHAVELINK")
    private String ishavelink;

    @Column(name = "LINKEVENT")
    private String linkevent;

    @Column(name = "ISQUERYITEM")
    private String isqueryitem;

    @Column(name = "QUERYEVENT")
    private String queryevent;

    @Column(name = "ISPRINT")
    private String isprint;

    @Column(name = "COLSPAN")
    private Short colspan;

    @Column(name = "QUERYCOLSPAN")
    private Short querycolspan;

    @Column(name = "STYLE")
    private String style;

    @Column(name = "ISNULL")
    private String isnull;

    @Column(name = "ALIGN")
    private String align;

    @Column(name = "ISORDER")
    private String isorder;

    @Column(name = "ISSUM")
    private String issum;

    @Column(name = "EDITFIELD")
    private String editfield;

    @Column(name = "TABLEFIELD")
    private String tablefield;

    @Column(name = "QUERYFIELD")
    private String queryfield;

    @Column(name = "ISQUERYCONFIG")
    private String isqueryconfig;

    @Column(name = "ISTABLECONFIG")
    private String istableconfig;

    @Column(name = "ISEDITCONFIG")
    private String iseditconfig;

    /**
     * @return TEMPLETID
     */
    public String getTempletid() {
        return templetid;
    }

    /**
     * @param templetid
     */
    public void setTempletid(String templetid) {
        this.templetid = templetid;
    }

    /**
     * @return TABLEID
     */
    public String getTableid() {
        return tableid;
    }

    /**
     * @param tableid
     */
    public void setTableid(String tableid) {
        this.tableid = tableid;
    }

    /**
     * @return FIELDCODE
     */
    public String getFieldcode() {
        return fieldcode;
    }

    /**
     * @param fieldcode
     */
    public void setFieldcode(String fieldcode) {
        this.fieldcode = fieldcode;
    }

    /**
     * @return TABINDEX
     */
    public Double getTabindex() {
        return tabindex;
    }

    /**
     * @param tabindex
     */
    public void setTabindex(Double tabindex) {
        this.tabindex = tabindex;
    }

    /**
     * @return NAME
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * @return LENGTH
     */
    public Double getLength() {
        return length;
    }

    /**
     * @param length
     */
    public void setLength(Double length) {
        this.length = length;
    }

    /**
     * @return PROPERTY
     */
    public String getProperty() {
        return property;
    }

    /**
     * @param property
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * @return MIN_VALUE
     */
    public String getMinValue() {
        return minValue;
    }

    /**
     * @param minValue
     */
    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    /**
     * @return MAX_VALUE
     */
    public String getMaxValue() {
        return maxValue;
    }

    /**
     * @param maxValue
     */
    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * @return FORMAT
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @return RESCODE
     */
    public String getRescode() {
        return rescode;
    }

    /**
     * @param rescode
     */
    public void setRescode(String rescode) {
        this.rescode = rescode;
    }

    /**
     * @return SOURCE
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return REALFIELD
     */
    public String getRealfield() {
        return realfield;
    }

    /**
     * @param realfield
     */
    public void setRealfield(String realfield) {
        this.realfield = realfield;
    }

    /**
     * @return TAGTYPE
     */
    public String getTagtype() {
        return tagtype;
    }

    /**
     * @param tagtype
     */
    public void setTagtype(String tagtype) {
        this.tagtype = tagtype;
    }

    /**
     * @return OPERATOR
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return ISEDIT
     */
    public String getIsedit() {
        return isedit;
    }

    /**
     * @param isedit
     */
    public void setIsedit(String isedit) {
        this.isedit = isedit;
    }

    /**
     * @return ISREADONLY
     */
    public String getIsreadonly() {
        return isreadonly;
    }

    /**
     * @param isreadonly
     */
    public void setIsreadonly(String isreadonly) {
        this.isreadonly = isreadonly;
    }

    /**
     * @return EDITEVENT
     */
    public String getEditevent() {
        return editevent;
    }

    /**
     * @param editevent
     */
    public void setEditevent(String editevent) {
        this.editevent = editevent;
    }

    /**
     * @return ISLIST
     */
    public String getIslist() {
        return islist;
    }

    /**
     * @param islist
     */
    public void setIslist(String islist) {
        this.islist = islist;
    }

    /**
     * @return ISDISPLAY
     */
    public String getIsdisplay() {
        return isdisplay;
    }

    /**
     * @param isdisplay
     */
    public void setIsdisplay(String isdisplay) {
        this.isdisplay = isdisplay;
    }

    /**
     * @return ISHAVELINK
     */
    public String getIshavelink() {
        return ishavelink;
    }

    /**
     * @param ishavelink
     */
    public void setIshavelink(String ishavelink) {
        this.ishavelink = ishavelink;
    }

    /**
     * @return LINKEVENT
     */
    public String getLinkevent() {
        return linkevent;
    }

    /**
     * @param linkevent
     */
    public void setLinkevent(String linkevent) {
        this.linkevent = linkevent;
    }

    /**
     * @return ISQUERYITEM
     */
    public String getIsqueryitem() {
        return isqueryitem;
    }

    /**
     * @param isqueryitem
     */
    public void setIsqueryitem(String isqueryitem) {
        this.isqueryitem = isqueryitem;
    }

    /**
     * @return QUERYEVENT
     */
    public String getQueryevent() {
        return queryevent;
    }

    /**
     * @param queryevent
     */
    public void setQueryevent(String queryevent) {
        this.queryevent = queryevent;
    }

    /**
     * @return ISPRINT
     */
    public String getIsprint() {
        return isprint;
    }

    /**
     * @param isprint
     */
    public void setIsprint(String isprint) {
        this.isprint = isprint;
    }

    /**
     * @return COLSPAN
     */
    public Short getColspan() {
        return colspan;
    }

    /**
     * @param colspan
     */
    public void setColspan(Short colspan) {
        this.colspan = colspan;
    }

    /**
     * @return QUERYCOLSPAN
     */
    public Short getQuerycolspan() {
        return querycolspan;
    }

    /**
     * @param querycolspan
     */
    public void setQuerycolspan(Short querycolspan) {
        this.querycolspan = querycolspan;
    }

    /**
     * @return STYLE
     */
    public String getStyle() {
        return style;
    }

    /**
     * @param style
     */
    public void setStyle(String style) {
        this.style = style;
    }

    /**
     * @return ISNULL
     */
    public String getIsnull() {
        return isnull;
    }

    /**
     * @param isnull
     */
    public void setIsnull(String isnull) {
        this.isnull = isnull;
    }

    /**
     * @return ALIGN
     */
    public String getAlign() {
        return align;
    }

    /**
     * @param align
     */
    public void setAlign(String align) {
        this.align = align;
    }

    /**
     * @return ISORDER
     */
    public String getIsorder() {
        return isorder;
    }

    /**
     * @param isorder
     */
    public void setIsorder(String isorder) {
        this.isorder = isorder;
    }

    /**
     * @return ISSUM
     */
    public String getIssum() {
        return issum;
    }

    /**
     * @param issum
     */
    public void setIssum(String issum) {
        this.issum = issum;
    }

    /**
     * @return EDITFIELD
     */
    public String getEditfield() {
        return editfield;
    }

    /**
     * @param editfield
     */
    public void setEditfield(String editfield) {
        this.editfield = editfield;
    }

    /**
     * @return TABLEFIELD
     */
    public String getTablefield() {
        return tablefield;
    }

    /**
     * @param tablefield
     */
    public void setTablefield(String tablefield) {
        this.tablefield = tablefield;
    }

    /**
     * @return QUERYFIELD
     */
    public String getQueryfield() {
        return queryfield;
    }

    /**
     * @param queryfield
     */
    public void setQueryfield(String queryfield) {
        this.queryfield = queryfield;
    }

    /**
     * @return ISQUERYCONFIG
     */
    public String getIsqueryconfig() {
        return isqueryconfig;
    }

    /**
     * @param isqueryconfig
     */
    public void setIsqueryconfig(String isqueryconfig) {
        this.isqueryconfig = isqueryconfig;
    }

    /**
     * @return ISTABLECONFIG
     */
    public String getIstableconfig() {
        return istableconfig;
    }

    /**
     * @param istableconfig
     */
    public void setIstableconfig(String istableconfig) {
        this.istableconfig = istableconfig;
    }

    /**
     * @return ISEDITCONFIG
     */
    public String getIseditconfig() {
        return iseditconfig;
    }

    /**
     * @param iseditconfig
     */
    public void setIseditconfig(String iseditconfig) {
        this.iseditconfig = iseditconfig;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

}