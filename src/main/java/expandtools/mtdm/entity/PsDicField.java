package expandtools.mtdm.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name = "ps_dic_field")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PsDicField {
    @Id
    private String tableid;
    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "LENGTH")
    private Double length;

    @Column(name = "ISPK")
    private String ispk;

    @Column(name = "ISNULL")
    private String isnull;

    @Column(name = "ISVIRTUAL")
    private String isvirtual;

    @Column(name = "FORMAT")
    private String format;

    @Column(name = "RESCODE")
    private String rescode;

    @Column(name = "PROPERTY")
    private String property;

    @Column(name = "SOURCE")
    private String source;

    @Column(name = "MIN_VALUE")
    private String minValue;

    @Column(name = "MAX_VALUE")
    private String maxValue;

    @Column(name = "DEFVALUE")
    private String defvalue;

    @Column(name = "MEMO")
    private String memo;

    /**
     * @return tableid
     */
    public String getTableid() {
        return tableid;
    }

    /**
     * @param tableid
     */
    public void setTableid(String tableid) {
        this.tableid = tableid;
    }

    /**
     * @return CODE
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return NAME
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return TYPE
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return LENGTH
     */
    public Double getLength() {
        return length;
    }

    /**
     * @param length
     */
    public void setLength(Double length) {
        this.length = length;
    }

    /**
     * @return ISPK
     */
    public String getIspk() {
        return ispk;
    }

    /**
     * @param ispk
     */
    public void setIspk(String ispk) {
        this.ispk = ispk;
    }

    /**
     * @return ISNULL
     */
    public String getIsnull() {
        return isnull;
    }

    /**
     * @param isnull
     */
    public void setIsnull(String isnull) {
        this.isnull = isnull;
    }

    /**
     * @return ISVIRTUAL
     */
    public String getIsvirtual() {
        return isvirtual;
    }

    /**
     * @param isvirtual
     */
    public void setIsvirtual(String isvirtual) {
        this.isvirtual = isvirtual;
    }

    /**
     * @return FORMAT
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @return RESCODE
     */
    public String getRescode() {
        return rescode;
    }

    /**
     * @param rescode
     */
    public void setRescode(String rescode) {
        this.rescode = rescode;
    }

    /**
     * @return PROPERTY
     */
    public String getProperty() {
        return property;
    }

    /**
     * @param property
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * @return SOURCE
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return MIN_VALUE
     */
    public String getMinValue() {
        return minValue;
    }

    /**
     * @param minValue
     */
    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    /**
     * @return MAX_VALUE
     */
    public String getMaxValue() {
        return maxValue;
    }

    /**
     * @param maxValue
     */
    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * @return DEFVALUE
     */
    public String getDefvalue() {
        return defvalue;
    }

    /**
     * @param defvalue
     */
    public void setDefvalue(String defvalue) {
        this.defvalue = defvalue;
    }

    /**
     * @return MEMO
     */
    public String getMemo() {
        return memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }
}