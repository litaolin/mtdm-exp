package expandtools.mtdm.entity;

import javax.persistence.*;

@Table(name = "ps_dic_table")
public class PsDicTable {
    @Id
    private String id;

    private String code;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ALIAS")
    private String alias;

    @Column(name = "MEMO")
    private String memo;

    @Column(name = "MODULECODE")
    private String modulecode;

    @Column(name = "OWNER")
    private String owner;

    @Column(name = "real_id")
    private String realId;

    @Column(name = "TYPE")
    private String type;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return NAME
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return ALIAS
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return MEMO
     */
    public String getMemo() {
        return memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return MODULECODE
     */
    public String getModulecode() {
        return modulecode;
    }

    /**
     * @param modulecode
     */
    public void setModulecode(String modulecode) {
        this.modulecode = modulecode;
    }

    /**
     * @return OWNER
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return real_id
     */
    public String getRealId() {
        return realId;
    }

    /**
     * @param realId
     */
    public void setRealId(String realId) {
        this.realId = realId;
    }

    /**
     * @return TYPE
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }
}