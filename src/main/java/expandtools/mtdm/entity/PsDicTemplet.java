package expandtools.mtdm.entity;

import javax.persistence.*;

@Table(name = "ps_dic_templet")
public class PsDicTemplet {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private String id;

    private String code;

    private String name;

    @Column(name = "MEMO")
    private String memo;

    @Column(name = "TABLEID")
    private String tableid;

    @Column(name = "MODULECODE")
    private String modulecode;

    private String flag;

    private String content;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return MEMO
     */
    public String getMemo() {
        return memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return TABLEID
     */
    public String getTableid() {
        return tableid;
    }

    /**
     * @param tableid
     */
    public void setTableid(String tableid) {
        this.tableid = tableid;
    }

    /**
     * @return MODULECODE
     */
    public String getModulecode() {
        return modulecode;
    }

    /**
     * @param modulecode
     */
    public void setModulecode(String modulecode) {
        this.modulecode = modulecode;
    }

    /**
     * @return flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * @param flag
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }
}