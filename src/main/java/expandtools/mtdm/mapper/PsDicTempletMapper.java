package expandtools.mtdm.mapper;

import expandtools.mtdm.entity.PsDicTemplet;
import tk.mybatis.mapper.common.BaseMapper;

public interface PsDicTempletMapper extends BaseMapper<PsDicTemplet> {
}