package expandtools.mtdm.mapper;

import expandtools.mtdm.entity.PsDicTempletInfo;
import expandtools.mtdm.service.PsDicTempletInfoService;
import tk.mybatis.mapper.common.BaseMapper;
public interface PsDicTempletInfoMapper extends BaseMapper<PsDicTempletInfo> {

}