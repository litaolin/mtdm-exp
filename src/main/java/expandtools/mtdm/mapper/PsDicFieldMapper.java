package expandtools.mtdm.mapper;

import expandtools.mtdm.entity.PsDicField;
import tk.mybatis.mapper.common.BaseMapper;

public interface PsDicFieldMapper extends BaseMapper<PsDicField> {
}