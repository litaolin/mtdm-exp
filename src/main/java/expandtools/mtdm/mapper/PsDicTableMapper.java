package expandtools.mtdm.mapper;

import expandtools.mtdm.entity.PsDicTable;
import tk.mybatis.mapper.common.BaseMapper;

public interface PsDicTableMapper extends BaseMapper<PsDicTable> {
}